$($ => 
{
  let ws = new WebSocket('ws://localhost:9002/hello', 'v1');

  ws.addEventListener ('open', (event) => 
  {
    console.log('Opened : ', ws.protocol);
    ws.send (JSON.stringify ({cmd:"join", name:"John"}));    
    ws.send (JSON.stringify ({cmd:"shoot", x: 0, y: 0}));    
  });

  ws.addEventListener('message', (event) => 
  {    
    let packet = JSON.parse (event.data);
    console.log(packet);
    if (packet.cmd == "walk")
    {
      let {cmd, x, y} = packet;
      y = parseInt (y);
      x = parseInt (x);
      let offs = [
        {dx: +1, dy: +1},
        {dx: +0, dy: +1},
        {dx: -1, dy: +1},
        {dx: +1, dy: +0},
        {dx: +0, dy: +0},
        {dx: -1, dy: +0},
        {dx: +1, dy: -1},
        {dx: +0, dy: -1},
        {dx: -1, dy: -1},
        {dx: +0, dy: +0}
      ];
      //for (let {dx, dy} of offs)
      //{
        //ws.send (JSON.stringify ({"cmd": "shoot", "x": x + dx, "y": y + dy}));          
      //}
    }
    else if (packet.cmd == "boom")
    {
      let {cmd, shooter, success} = packet;
      //ws.close();
    }
  });

});