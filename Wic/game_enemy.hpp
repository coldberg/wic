#pragma once

#include <cstdint>
#include <cstddef>
#include <random>

namespace wic
{
  struct game_enemy
  {
    template <typename X, typename A, typename B>
    static auto clamp(X x, A a, B b)
    {
      using T = std::common_type_t<X, A, B>;
      if (x < a) return T (a);
      if (x > b) return T (b);
      return T (x);
    }

    auto tick ()->std::tuple<int, int>;

    bool shoot (int x, int y) const;

    bool has_won () const;

    static auto get_seed () -> std::uint64_t;

    game_enemy ();

  private:
    std::mt19937_64 dice_;
    std::int32_t x_, y_;
  };
}
