#include "game_server.hpp"
#include "game_session.hpp"

wic::game_server::game_server ()
{
  using std::placeholders::_1;
  using std::placeholders::_2;

  init_asio ();
  set_access_channels (log::alevel::fail);
  set_error_channels (log::elevel::rerror|log::elevel::fatal);

  const auto vld = &game_server::handle_validate;
  const auto opn = &game_server::handle_connect;
  set_validate_handler (std::bind (vld, this, _1));
  set_open_handler (std::bind (opn, this, _1));
}

bool wic::game_server::handle_validate (connection_hdl handle)
{
  auto conn = get_con_from_hdl (handle);
  const auto& subs = conn->get_requested_subprotocols ();
  for (const auto& s : subs)
  {
    conn->select_subprotocol (s);
    return true;
  }
  return true;
}

void wic::game_server::handle_connect (connection_hdl handle)
{
  try
  {
    auto sess = std::make_shared<game_session> (*this);  
    sess->set_connection(get_con_from_hdl (handle));
  }
  catch (const std::exception& ex)
  {
    get_elog().write(log::elevel::rerror, ex.what());
  }
}


void wic::game_server::run ()
{
  super::listen (9002);
  super::start_accept ();
  super::run();
}
