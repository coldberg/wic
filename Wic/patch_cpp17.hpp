#pragma once

#include <functional>
#include <memory>

namespace std
{
  template <typename _Arg1, typename _Result>
  using unary_function = std::function<_Result (_Arg1)>;

  template <typename _Arg1, typename _Arg2, typename _Result>
  using binary_function = std::function<_Result (_Arg1, _Arg2)>;

  template <typename T>
  using auto_ptr = std::unique_ptr<T>;
}
