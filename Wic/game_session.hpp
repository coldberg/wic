#pragma once

#include <websocketpp/server.hpp>
#include <websocketpp/config/asio.hpp>
#include <websocketpp/connection.hpp>
#include <boost/property_tree/ptree.hpp>
#include "game_enemy.hpp"

namespace wic
{
  using namespace websocketpp;

  struct game_server;

  struct game_session: 
    public std::enable_shared_from_this<game_session>
  {
    using connection_hdl = ::websocketpp::connection_hdl;
    using message_ptr = server<config::asio>::message_ptr;
    using connection_ptr = server<config::asio>::connection_ptr;
    using json_type = boost::property_tree::ptree;

    game_session (game_server& server);
    void handle_message (connection_hdl handle, message_ptr msg);
    void handle_close (connection_hdl handle);

    static auto parse_json (const std::string& str) -> json_type;
    static auto write_json (const json_type& jsn) -> std::string;

    void set_connection(connection_ptr socket);
    void dispatch_message(const std::string&, const json_type&);

    void handle_bad_cmd (const json_type& params);
    void handle_join    (const json_type& params);
    void handle_shoot    (const json_type& params);

    void tick (const boost::system::error_code& ec);
  private:
    game_server& server_;
    connection_ptr socket_;

    std::string name_;
    std::unique_ptr<game_enemy> enemy_;

    boost::asio::steady_timer timer_;
  };

}