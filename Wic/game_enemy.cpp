#include "game_enemy.hpp"

auto wic::game_enemy::tick () -> std::tuple<int, int>
{
  std::uniform_int_distribution<int> dist_x_ (-1 - (x_ > 5 ? 1 : 0), 1 + (x_ < 5 ? 1 : 0));
  std::uniform_int_distribution<int> dist_y_ (-1, 3);

  auto nx_ = clamp (x_ + clamp (dist_x_ (dice_), -3, 3), 0, 9);
  auto ny_ = clamp (y_ + clamp (dist_y_ (dice_), -1, 1), 0, 30);
  return std::exchange (std::tie (x_, y_), std::tie (nx_, ny_));
}

bool wic::game_enemy::shoot (int x, int y) const
{
  return x_ == x && y_ == y;
}

bool wic::game_enemy::has_won () const
{
  return y_ >= 30;
}

auto wic::game_enemy::get_seed () ->std::uint64_t
{
  static std::random_device rd_;
  return rd_ ();
}

wic::game_enemy::game_enemy ():
  dice_ (get_seed ()),
  x_ (dice_ () % 10),
  y_ (0)
{}
