#pragma once

#include <websocketpp/server.hpp>
#include <websocketpp/config/asio.hpp>
#include <boost/asio.hpp>

namespace wic
{
  using namespace websocketpp;


  struct game_server:
    server<config::asio>
  {
    using super = server<config::asio>;
    using connection_hdl = ::websocketpp::connection_hdl;

    game_server ();

    bool handle_validate (connection_hdl handle);
    void handle_connect  (connection_hdl handle);

    void run ();    
  };
}
