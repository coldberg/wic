#include "game_server.hpp"
#include "game_session.hpp"

int main ()
try
{
  wic::game_server gs;
  gs.run();

  return 0;
}
catch (std::exception& ex)
{
  std::cout << ex.what () << "\n";
  return -1;
}