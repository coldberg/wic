#include "game_session.hpp"
#include "game_server.hpp"

#include <boost/property_tree/json_parser.hpp>
#include <boost/lexical_cast.hpp>

#include <fmt/format.h>

#include <unordered_map>
#include <sstream>
#include <functional>
#include <memory>

wic::game_session::game_session (game_server& server):
  server_ (server),
  socket_ (nullptr),
  timer_ (server_.get_io_service ())
{}

void wic::game_session::handle_message (connection_hdl handle, message_ptr msg)
{
  try
  {
    auto data = parse_json (msg->get_payload ());
    dispatch_message (data.get ("cmd", "bad_cmd"), data);
  }
  catch (std::exception& ex)
  {
    server_.get_elog().write(log::elevel::rerror, ex.what());
  }
}

void wic::game_session::handle_close (connection_hdl handle)
{}

auto wic::game_session::parse_json (const std::string& str) -> json_type
{
  using namespace boost::property_tree;
  std::istringstream ss (str);
  json_type data;
  json_parser::read_json (ss, data);
  return data;
}

auto wic::game_session::write_json (const json_type& jsn) -> std::string
{
  using namespace boost::property_tree;
  std::ostringstream ss;
  json_parser::write_json (ss, jsn);
  return ss.str ();
}

void wic::game_session::set_connection (connection_ptr socket)
{
  using std::placeholders::_1;
  using std::placeholders::_2;

  socket_ = socket;  
  const auto m = &game_session::handle_message;
  const auto c = &game_session::handle_close;
  auto this_ = shared_from_this ();
  socket_->set_message_handler (std::bind (m, this_, _1, _2));
  socket_->set_close_handler (std::bind (c, this_, _1));
}

void wic::game_session::dispatch_message (const std::string& cmd, const json_type& data)
{
  using std::placeholders::_1;
  using std::placeholders::_2;
  using namespace std::string_literals;

  static const auto mkcmd = [] (auto&& m)
  {
    return std::bind (std::forward<decltype(m)> (m), _1, _2);
  };

  using cmd_fun_type = std::function<void (game_session&, const json_type&)>;
  using cmd_tbl_type = std::unordered_map<std::string, cmd_fun_type>;
  using G = game_session;

  static const cmd_tbl_type cmd_tbl =
  {
    {"bad_cmd"s, mkcmd (&G::handle_bad_cmd)},
    {"join"s,    mkcmd (&G::handle_join)},
    {"shoot"s,   mkcmd (&G::handle_shoot)},
  };
  cmd_tbl.at (!cmd_tbl.count (cmd) ? "bad_cmd"s : cmd) (*this, data);
}

void wic::game_session::handle_bad_cmd (const json_type& params)
{
  const auto msg = fmt::format ("Bad command `{0}`!", params.get ("id", ""));
  server_.get_elog ().write (log::elevel::warn, msg.c_str ());
}

void wic::game_session::tick (const boost::system::error_code& o)
{
  using std::placeholders::_1;
  boost::system::error_code ec;
  json_type packet;
  try
  {
    auto [x, y] = enemy_->tick ();
    if (!enemy_->has_won ())
    {
      packet.put ("cmd", "walk");
      packet.put ("x", x);
      packet.put ("y", y);
      socket_->send (write_json (packet));
      timer_.expires_from_now (std::chrono::seconds (2), ec);
      timer_.async_wait (std::bind (&game_session::tick, this, _1));
    }
    else
    {
      packet.put ("cmd", "game_over");
      socket_->send (write_json (packet));
      socket_->close (close::status::normal, "Game over");
    }
  }
  catch (const std::exception& ex)
  {
    server_.get_elog().write(log::elevel::rerror, ex.what());
  }
}

void wic::game_session::handle_join (const json_type& params)
{
  name_ = params.get ("name", "[JohnDoe]");
  boost::system::error_code ec;
  enemy_ = std::make_unique<game_enemy> ();
  timer_.expires_from_now (std::chrono::seconds (2), ec);
  tick (ec);
}

void wic::game_session::handle_shoot (const json_type& params)
{
  json_type packet;
  auto x_ = boost::lexical_cast<int>(params.get ("x", "0"));
  auto y_ = boost::lexical_cast<int>(params.get ("y", "0"));
  packet.put ("cmd", "boom");
  packet.put ("shooter", name_);
  if (enemy_->shoot (x_, y_))
  {
    packet.put ("success", 1);
    enemy_ = std::make_unique<game_enemy> ();
    return;
  }
  packet.put ("success", 0);
  socket_->send(write_json (packet));
}

